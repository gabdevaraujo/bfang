import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './template/header/header.component';

import {MenubarModule} from 'primeng/menubar';
import { ShowComponent } from './template/show/show.component';

@NgModule({
  declarations: [
    AppComponent,
      HeaderComponent,
      ShowComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MenubarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
